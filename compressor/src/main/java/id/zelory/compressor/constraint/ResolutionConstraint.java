package id.zelory.compressor.constraint;

import id.zelory.compressor.Util;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created on : January 24, 2020
 * Author     : zetbaitsu
 * Name       : Zetra
 * GitHub     : https://github.com/zetbaitsu
 */
public class ResolutionConstraint implements Constraint {
    private final int width;
    private final int height;

    public ResolutionConstraint(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public boolean isSatisfied(File imageFile) {
        boolean result = false;
        try {
            ImageSource imageSource = ImageSource.create(imageFile.getCanonicalFile(), null);
            Size size = imageSource.getImageInfo().size;
            result = Util.calculateInSampleSize(size, width, height) <= 1;
        } catch (IOException e) {
            Logger.getLogger(ResolutionConstraint.class.getName()).log(Level.WARNING, e.getMessage());
        }
        return result;
    }

    public File satisfy(File imageFile) {
        PixelMap bitmap = Util.decodeSampledBitmapFromFile(imageFile, width, height);
        return Util.overWrite(imageFile, bitmap, null, 0);
    }
}
