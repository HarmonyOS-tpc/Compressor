package id.zelory.compressor;

import id.zelory.compressor.constraint.CompressFormat;
import id.zelory.compressor.extutil.Intrinsics;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.media.image.ImagePacker;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Util {
    private static final Logger logger = Logger.getLogger(Util.class.getName());
    private static CompressFormat compressFormat;

    private static String cachePath(Context context) {
        StringBuilder path = new StringBuilder();
        File cacheDir = context.getCacheDir();
        Intrinsics.checkExpressionValueIsNotNull(cacheDir, "context.cacheDir");
        return path.append(cacheDir.getPath()).append(File.separator).append("compressor").append(File.separator).toString();
    }

    public static CompressFormat compressFormat() {
        Intrinsics.checkParameterIsNotNull(compressFormat, "Util.compressFormat");
        return compressFormat;
    }

    public static CompressFormat compressFormat(File imageFile) {
        Intrinsics.checkParameterIsNotNull(imageFile, "imageFile");
        String fileName = imageFile.getName();
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
        if ("png".equals(extension)) {
            compressFormat = CompressFormat.PNG;
        } else if ("webp".equals(extension)) {
            compressFormat = CompressFormat.WEBP;
        } else {
            compressFormat = CompressFormat.JPEG;
        }
        return compressFormat;
    }

    public static PixelMap loadBitmap(File imageFile) {
        try {
            Intrinsics.checkParameterIsNotNull(imageFile, "imageFile");
            ImageSource imageSource = ImageSource.create(imageFile.getCanonicalFile(), null);
            PixelMap bitmap = imageSource.createPixelmap(null);
            ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
            decodingOpts.rotateDegrees = determineImageRotation(imageFile);
            decodingOpts.desiredSize = bitmap.getImageInfo().size;
            PixelMap result = imageSource.createPixelmap(decodingOpts);
            Intrinsics.checkExpressionValueIsNotNull(result, "result");
            return result;
        } catch (IOException e) {
            return null;
        }
    }

    public static PixelMap decodeSampledBitmapFromFile(File imageFile, int reqWidth, int reqHeight) {
        try {
            Intrinsics.checkParameterIsNotNull(imageFile, "imageFile");
            ImageSource imageSource = ImageSource.create(imageFile.getCanonicalFile(), null);
            Size size = imageSource.getImageInfo().size;
            ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
            decodingOpts.sampleSize = calculateInSampleSize(size, reqWidth, reqHeight);
            decodingOpts.rotateDegrees = determineImageRotation(imageFile);
            decodingOpts.desiredSize = new Size(reqWidth, reqHeight);
            PixelMap bitmap = imageSource.createPixelmap(decodingOpts);
            Intrinsics.checkExpressionValueIsNotNull(bitmap, "BitmapFactory.decodeFile…eFile.absolutePath, this)");
            Intrinsics.checkExpressionValueIsNotNull(bitmap, "BitmapFactory.Options().…absolutePath, this)\n    }");
            return bitmap;
        } catch (IOException e) {
            return null;
        }
    }

    public static int calculateInSampleSize(Size size, int reqWidth, int reqHeight) {
        Intrinsics.checkParameterIsNotNull(size, "size");
        int height = size.height;
        int width = size.width;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height / 2;
            int halfWidth = width / 2;
            while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static float determineImageRotation(File imageFile) {
        return 0.0f;
    }

    public static File copyToCache(Context context, File imageFile) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(imageFile, "imageFile");
        File result = null;
        File path = new File(cachePath(context));
        if (!path.exists()) {
            boolean isE = path.mkdirs();
        }
        result = new File(cachePath(context) + imageFile.getName());
        try {
            copyFile(imageFile, result);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }
        return result;
    }

    public static File overWrite(File imageFile, PixelMap bitmap, CompressFormat format, int quality) {
        Intrinsics.checkParameterIsNotNull(imageFile, "imageFile");
        Intrinsics.checkParameterIsNotNull(bitmap, "bitmap");
        if (format == null) {
            format = compressFormat(imageFile);
        }
        if (quality == 0) {
            quality = 100;
        }
        File file = null;
        if (format == compressFormat(imageFile)) {
            file = imageFile;
        } else {
            try {
                StringBuilder path = new StringBuilder();
                String imagePath = imageFile.getCanonicalPath();
                Intrinsics.checkExpressionValueIsNotNull(imagePath, "imageFile.absolutePath");
                file = new File(path.append(imagePath.substring(0, imagePath.lastIndexOf("."))).append('.').append(format.getName()).toString());
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
        File result = file;
        boolean isDelte = imageFile.delete();
        saveBitmap(bitmap, result, format, quality);
        return result;
    }

    public static File overWriteDefault(File file, PixelMap bitmap, CompressFormat format, int quality, int flag, Object obj) {
        if ((flag & 4) != 0) {
            format = compressFormat(file);
        }
        if ((flag & 8) != 0) {
            quality = 100;
        }
        return overWrite(file, bitmap, format, quality);
    }

    public static void saveBitmap(PixelMap bitmap, File destination, CompressFormat format, int quality) {
        Intrinsics.checkParameterIsNotNull(bitmap, "bitmap");
        Intrinsics.checkParameterIsNotNull(destination, "destination");
        File file = destination.getParentFile();
        if (file != null) {
            boolean isE = file.mkdirs();
        }
        try (
                FileOutputStream fileOutputStream = new FileOutputStream(destination.getCanonicalFile());
        ) {
            ImagePacker imagePacker = ImagePacker.create();
            ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
            packingOptions.format = "image/jpeg";
            packingOptions.quality = quality;
            imagePacker.initializePacking(fileOutputStream, packingOptions);
            imagePacker.addImage(bitmap);
            imagePacker.finalizePacking();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }

    }

    public static void saveBitmapDefault(
            PixelMap bitmap, File file, CompressFormat format, int quality,
            int flag, Object obj) {
        if ((flag & 4) != 0) {
            format = compressFormat(file);
        }
        if ((flag & 8) != 0) {
            quality = 100;
        }
        saveBitmap(bitmap, file, format, quality);
    }

    public static void copyFile(File source, File dest) {
        try (
                FileInputStream input = new FileInputStream(source);
                FileOutputStream output = new FileOutputStream(dest);
        ) {
            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = input.read(buf)) > 0) {
                output.write(buf, 0, bytesRead);
            }
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
    }

    public static File resPathToFile(Context context, String resPath) {
        String[] fileNames = resPath.split(""+File.separatorChar);
        File tempFile = new File(context.getCacheDir() + File.separator + fileNames[fileNames.length - 1]);
        ResourceManager resourceManager = context.getResourceManager();
        try (
                Resource resource = resourceManager.getRawFileEntry(resPath).openRawFile();
                FileOutputStream fos = new FileOutputStream(tempFile);
        ) {
            byte[] buffer = new byte[resource.available()];
            resource.read(buffer);
            fos.write(buffer);
        } catch (IOException o) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, o.getMessage());
        }
        return tempFile;
    }
}

