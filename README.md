Compressor
======
Compressor is a lightweight and powerful harmony image compression library. Compressor will allow you to compress large photos into smaller sized photos with very less or negligible loss in quality of the image.
WebP is not supported.

<img src="https://gitee.com/openharmony-tpc/Compressor/raw/master/gifs/compressor.gif" width="50%" height="50%"/>

## Usage

### Compress Image File
```java
Compressor compressor = new Compressor();
compressor.compress(getContext(), file, null);
```

### I want custom Compressor!
```java
 ImageSource imageSource = ImageSource.create(file, null);
        Size size = imageSource.getImageInfo().size;
        Compressor compressor = new Compressor();
        Compression compression = new Compression();
        compression.resolution(size.width, size.height);
        compression.quality(80);
        compression.format(CompressFormat.JPEG);
        compression.size(2048, 0, 0);
        compressor.compress(getContext(), file, compression);
```

## entry运行要求
通过DevEco studio,并下载openharmonySDK
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即您当前IDE新建项目中所用的版本）

## 集成
方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:Compressor:1.0.1'

# License
Copyright (c) 2016 Zetra.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.